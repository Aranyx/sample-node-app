# Welcome to Sample Node App 👋
[![Version](https://img.shields.io/npm/v/Sample Node App.svg)](https://www.npmjs.com/package/Sample Node App)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)

> sample node app

### 🏠 [Homepage](https://bitbucket.org/Aranyx/sample-node-app#readme)

## Install

```sh
npm install
```

## Run tests

```sh
npm run test
```

## Author

👤 **Aravind Kumar Selvakumar**

## License Info
The MIT License (MIT)- grants full permission to use my project with or without any modifications


## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://bitbucket.org/Aranyx/sample-node-app/issues). You can also take a look at the [contributing guide](https://Aranyx@bitbucket.org/Aranyx/sample-node-app/blob/master/CONTRIBUTING.md).
