var express = require('express');
const port = 3000;
var app = express();
app.get('/', function (req, res) {
    res.send('Hello World!');
});
var server = app.listen(port, function () {
    console.log(`Sample app listening at http://localhost:${port}`)
});